ENV['RACK_ENV'] ||= 'test'

require File.expand_path('../application', __FILE__)
require File.expand_path('../redis', __FILE__)