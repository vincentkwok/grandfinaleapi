class Event < Ohm::Model
  class Entity < Grape::Entity
    expose :id
    expose :task
    expose :is_finished
    expose :is_active
  end

  attribute :task
  attribute :is_finished
  attribute :is_active

end