module Acme
  class Event < Grape::API
    format :json

    resource :events do
      # TODO: active events
      desc 'Get all events', {
        :nickname => 'getEvents',
        :http_codes => [[200]]
      }
      get do
        events = ::Event.all
        present events.to_a
      end

      desc 'Create a new event', {
        :nickname => 'postEvent',
        :http_codes => [[200]]
      }
      params do
        requires :task, type: String, desc: 'task content'
      end
      post do
        # weird bug when update/insert boolean
        event = ::Event.create(task: declared(params)['task'],
                               is_finished: "false",
                               is_active: "true")
        present event
      end

      desc '(Dev use) Remove all events', {
        :nickname => 'deleteEvent',
        :http_codes => [[200]]
      }
      delete do
        ::Event.all.map &:delete
        present "Clean all events"
      end

      params do
        requires :id, type: Integer, desc: 'event id'
        requires :is_finished, type: Boolean, desc: 'is event finished'
        requires :is_active, type: Boolean, desc: 'is event active'
      end
      route_param :id do
        desc 'update an event', {
          :nickname => 'putEvent',
          :http_codes => [[200]]
        }
        put do
          event = ::Event[declared(params)['id']]
          return present "No such event id" if event.nil?
          return present "Event is not active" if !event.is_active

          # weird bug when update/insert boolean
          event.update(is_finished: "#{declared(params)['is_finished']}", is_active: "#{declared(params)['is_active']}")

          present event
        end
      end

    end
  end
end